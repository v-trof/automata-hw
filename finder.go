package main

import (
	"io/ioutil"
	"log"
)

// Transitions is a map of transition: state: map[letter: new state]
type Transitions = map[int]map[rune]int

// DeterministicAutomaton to be use in runners
type DeterministicAutomaton struct {
	transitions   Transitions
	terminalState int
	state         int
}

func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func makeTextMatcher(a DeterministicAutomaton) func(text string, results chan string) {
	initialState := a.state

	return func(text string, results chan string) {
		defer close(results)

		for from := range text {
			a.state = initialState

			for idx, c := range text[from:] {
				a.state = a.transitions[a.state][c]

				if a.state == 0 {
					break
				}

				if a.state == a.terminalState {
					results <- text[from : from+idx+1]
				}
			}
		}
	}
}

func makeStringTester(a DeterministicAutomaton) func(text string) bool {
	initialState := a.state

	return func(text string) bool {
		a.state = initialState

		for _, c := range text {
			a.state = a.transitions[a.state][c]
		}

		return a.terminalState == a.state
	}
}

func main() {
	transitions := Transitions{
		1: {'%': 2},
		2: {'+': 3},
		3: {'x': 4},
		4: {'+': 5},
		5: {'+': 3, '?': 6, '%': 7},
		6: {'?': 5},
		7: {},
	}

	appFinder := DeterministicAutomaton{transitions, 7, 1}

	assert(transitions)

	match := makeTextMatcher(appFinder)

	println("---In place example--")
	resultsInPlace := make(chan string)
	go match("word%+x+%word%+x+????%cool\neven newlines are fine %+x++x+??+x+????+x+%", resultsInPlace)

	for s := range resultsInPlace {
		println("Extracted from here |", s)
	}

	println()
	println("---File reading example--")
	data, err := ioutil.ReadFile("input.txt")
	if err != nil {
		println("Failed to open input.txt")
		log.Fatal(err)
	}
	text := string(data)

	resultsFile := make(chan string)

	go match(text, resultsFile)
	for s := range resultsFile {
		println("Extracted from file |", s)
	}
}

func assert(transitions Transitions) {
	appFinder := DeterministicAutomaton{transitions, 7, 1}

	test := makeStringTester(appFinder)

	// should work
	for _, s := range []string{
		"%+x+%",
		"%+x+??%",
		"%+x+????%",
		"%+x++x+%",
		"%+x++x+??+x+????+x+%",
	} {
		if !test(s) {
			panic("Tranition function it bad, it rejected " + s)
		}
	}

	// should fail
	for _, s := range []string{
		"+x+%",
		"%+x+??",
		"%??%",
		"%+x+???%",
		"%+x+?+x+%",
		"%+a++x+%",
		"word%+x+%",
		"%+x+%word",
	} {
		if test(s) {
			panic("Tranition function it bad, it accepted " + s)
		}
	}
}
